#!/usr/bin/env python3

from sys import argv
from math import tan, cos, acos, radians

if len(argv) < 4:
    print("Usage: ", argv[0], "<viewer_distance> <horizontal_angle> <vertical_angle>")
    exit(0)

# positive
viewer_distance = float(argv[1])
# between 0 and 90
horizontal_angle = float(argv[2])
# between -90 and 90
vertical_angle = float(argv[3])

print("Viewer distance:", viewer_distance)
print("Horizontal angle:", horizontal_angle)
print("Vertical angle:", vertical_angle)

# vertical_vanashing_point = viewer_distance / tan(radians(vertical_angle))
vertical_vanashing_point_offset = viewer_distance * tan(radians(90 - vertical_angle))
print("Vertical vanashing point offset:", round(vertical_vanashing_point_offset, 2))

horizontal_line_offset = viewer_distance * tan(radians(vertical_angle))
print("Horizontal line offset:", round(horizontal_line_offset, 2))

viewer_horizontal_line_distance = viewer_distance / cos(radians(vertical_angle))
# right_horizontal_vanashing_point_offset = viewer_horizontal_line_distance / tan(radians(horizontal_angle))
right_horizontal_vanashing_point_offset = viewer_horizontal_line_distance * tan(radians(90 - horizontal_angle))
left_horizontal_vanashing_point_offset = viewer_horizontal_line_distance * tan(radians(horizontal_angle))
print("Right vanishing point offset:", round(right_horizontal_vanashing_point_offset, 2))
print("Left vanishing point offset:", round(left_horizontal_vanashing_point_offset, 2))
